/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

import java.util.ArrayList;

public class MenuGroup
{

	private String name;
	private ArrayList<MenuItem> items;

	/**
	 * create a new menu group
	 *
	 * @param name
	 */
	public MenuGroup(String name)
	{
		this.name = name;
		this.items = new ArrayList<MenuItem>();
	}

	/**
	 * add an item to a menu group
	 *
	 * @param item
	 *
	 * @return boolean
	 */
	public boolean addItem(MenuItem item)
	{
		if(!items.contains(item))
		{
			items.add(item);
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * delete an item from a menu group
	 *
	 * @param item
	 *
	 * @return boolean
	 */
	public boolean deleteItem(MenuItem item)
	{
		if(items.contains(item))
		{
			items.remove(item);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void deleteItem(int index)
	{
		if(index < items.size())
		{
			items.remove(index);
		}
	}

	/**
	 * get the name of a menu group
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * get the array of items in a menu group
	 *
	 * @return the items
	 */
	public ArrayList<MenuItem> getItems()
	{
		return items;
	}

	public void setSpecial(int index)
	{
		if(index < items.size())
		{
			items.get(index).setSpecial();
		}
	}

	public void clearSpecials()
	{
		for(int i = 0; i < items.size(); i++)
		{
			this.items.get(i).notSpecial();
		}
	}

	public String toString()
	{
		return name;
	}
}
