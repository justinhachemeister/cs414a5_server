/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

public class Payment
{

	private String ccType;
	private int ccNumber;
	private int amount;
	private boolean paid; //for now, just assuming is paid when constructor called

	/**
	 * Constructor for a cash payment
	 *
	 * @param price
	 */
	public Payment(int price)
	{
		this.amount = price;
		this.paid = true;
	}

	/**
	 * Constructor for a cc payment
	 *
	 * @param ccType
	 * @param ccNumber
	 * @param price
	 */
	public Payment(String ccType, int ccNumber, int price)
	{
		this.setCcType(ccType);
		this.setCcNumber(ccNumber);
		this.amount = price;
		this.paid = true;
	}

	/**
	 * get the price of a payment
	 *
	 * @return the price
	 */
	public int getAmount()
	{
		return amount;
	}

	/** @return the paid */
	public boolean isPaid()
	{
		return paid;
	}

	/**
	 * @param paid
	 * 		the paid to set
	 */
	public void setPaid(boolean paid)
	{
		this.paid = paid;
	}

	public String getCcType()
	{
		return ccType;
	}

	public void setCcType(String ccType)
	{
		this.ccType = ccType;
	}

	public int getCcNumber()
	{
		return ccNumber;
	}

	public void setCcNumber(int ccNumber)
	{
		this.ccNumber = ccNumber;
	}
}
