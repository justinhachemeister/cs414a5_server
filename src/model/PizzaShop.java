/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

//TODO consider using singleton pattern if time permits

package model;

import java.util.ArrayList;

public class PizzaShop
{

	private Menu menu;
	private ArrayList<Order> orderList;
	private ArrayList<Employee> employeeList;
	private ArrayList<Customer> customerList;
	private int orderNum; //keeps running count of orders to generate order numbers
	private int employeeID;
	private int customerID;
	//private PizzaShopGUIHandler guiHandler;

	/** sets up an empty base for making a pizza shop */
	public PizzaShop()
	{
		this.menu = new Menu();

		//make pepperoni pizza
		MenuItem pizza1 = new MenuItem("Pepperoni", "Pizza with pepperoni on it.", 1000);
		MenuItem pizza2 = new MenuItem("Meat Lover's", "Pizza with lots of meat.", 1200);
		MenuItem pizza3 = new MenuItem("Veggie Lover's", "Pizza with lots of veggies.", 1200);
		//create pizza group and add pizzas
		MenuGroup pizzaGroup = new MenuGroup("pizza");
		pizzaGroup.addItem(pizza1);
		pizzaGroup.addItem(pizza2);
		pizzaGroup.addItem(pizza3);

		//create sodas
		MenuItem coke = new MenuItem("Coke", "Coke", 200);
		MenuItem dietCoke = new MenuItem("Diet Coke", "Diet Coke", 100);
		MenuItem pepsi = new MenuItem("Pepsi", "Pepsi", 100);
		MenuItem dietPepsi = new MenuItem("Diet Pepsi", "Diet Pepsi", 100);
		MenuItem sprite = new MenuItem("Sprite", "Sprite", 300);
		//create soda group and add sodas
		MenuGroup soda = new MenuGroup("soda");
		soda.addItem(coke);
		soda.addItem(dietCoke);
		soda.addItem(pepsi);
		soda.addItem(dietPepsi);
		soda.addItem(sprite);

		//create items and other group
		MenuItem breadSticks = new MenuItem("Bread Sticks", "Bread Sticks", 500);
		MenuItem garlicKnots = new MenuItem("Gralic Knots", "Garlic Knots", 600);
		MenuGroup other = new MenuGroup("other");
		other.addItem(breadSticks);
		other.addItem(garlicKnots);

		//add groups to menu
		this.menu.addGroup(pizzaGroup);
		this.menu.addGroup(soda);
		this.menu.addGroup(other);

		this.orderList = new ArrayList<Order>();
		this.employeeList = new ArrayList<Employee>();
		this.addEmployee("Kiosk", "123 Fake St", "Fort Collins", "Colorado", "80525", "303-718-7583", "A", "1");
		this.addEmployee("Casheer", "123 Fake St", "Fort Collins", "Colorado", "80525", "303-718-7583", "B", "2");
		this.addEmployee("Manager", "123 Fake St", "Fort Collins", "Colorado", "80525", "303-718-7583", "C", "3");
		this.employeeList.get(2).setManager();
		this.addEmployee("Chef", "123 Fake St", "Fort Collins", "Colorado", "80525", "303-718-7583", "C", "3");
		this.employeeList.get(3).setChef();
		this.customerList = new ArrayList<Customer>();
		this.orderNum = 0;
		this.customerID = 0;
		this.employeeID = 0;
		//this.guiHandler = new PizzaShopGUIHandler();
	}

	/*public PizzaShop(Menu existingMenu)
		 {
			  this.menu = existingMenu;
		 }*/

	/** @return the next orderNum */
	public int getNextOrderNum()
	{
		orderNum++;
		return orderNum;
	}

	/** @return the next employeeID */
	public int getNextEmployeeID()
	{
		employeeID++;
		return employeeID;
	}

	/** @return the next customerID */
	public int getNextCustomerID()
	{
		customerID++;
		return customerID;
	}

	/**
	 * sets the special for a menu
	 *
	 * @param index
	 */
	public void setPizzaSpecial(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		groups.get(0).setSpecial(index);
	}

	public void setDrinkSpecial(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		groups.get(1).setSpecial(index);
	}

	public void setOtherSpecial(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		groups.get(2).setSpecial(index);
	}

	public void clearSpecials()
	{
		this.menu.clearSpecials();
	}

	/**
	 * adds an order to the order list
	 *
	 * @param newOrder
	 */
	public void addOrder(Order newOrder)
	{
		this.orderList.add(newOrder);
	}

	/**
	 * set an order as finished
	 *
	 * @param orderIndex
	 */
	//public void finishOrder(int orderIndex)
	public void finishOrder(int index)
	{
		//TODO set order status to finished, may not want to delete.
		//potentially need to find orders by order or customer number, not array index
		this.orderList.get(index).setStatus(3);
	}

	public void removeOrder(int index)
	{
		this.orderList.remove(index);
	}

	/**
	 * get all orders that are not finished
	 *
	 * @return
	 */
	public ArrayList<Order> viewOrders()
	{
		//TODO check order status's and only return unfinished ones
		return orderList;
	}

	public void addEmployee(String name, String address, String city, String state,
	                        String zipCode, String phoneNumber, String loginID, String password)
	{
		employeeList.add(new Employee(name, address, city, state,
		                              zipCode, phoneNumber, loginID, password));
	}

	public void deleteEmployee(Employee e)
	{

	}

	public void addCustomer(String name, String address, String city, String state,
	                        String zipCode, String phoneNumber, int customerID)
	{
		customerList.add(new Customer(name, address, city, state,
		                              zipCode, phoneNumber, getNextCustomerID()));
	}

	public void deleteCustomer(Customer c)
	{

	}

	public int getCustomer(Customer c)
	{
		return customerList.indexOf(c);
	}

	public int getEmployee(Employee e)
	{
		return employeeList.indexOf(e);
	}

	public ArrayList<Employee> getEmployeeList()
	{
		return employeeList;
	}

	public ArrayList<Customer> getCustomerList()
	{
		return customerList;
	}

	public MenuGroup getPizzas()
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "pizza")
			{
				return groups.get(i);
			}
		}
		//if no pizzas group return an empty one.
		MenuGroup pizzas = new MenuGroup("pizza");
		return pizzas;

	}

	public MenuGroup getSodas()
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "soda")
			{
				return groups.get(i);
			}
		}
		//if no soda group return an empty one.
		MenuGroup sodas = new MenuGroup("soda");
		return sodas;

	}

	public MenuGroup getOthers()
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "other")
			{
				return groups.get(i);
			}
		}
		//if no other group return an empty one.
		MenuGroup others = new MenuGroup("other");
		return others;
	}

	public void RemovePizza(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "pizza")
			{
				groups.get(i).deleteItem(index);
			}
		}
	}

	public void RemoveSoda(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "soda")
			{
				groups.get(i).getItems().remove(index);
			}
		}
	}

	public void RemoveOther(int index)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "other")
			{
				groups.get(i).getItems().remove(index);
			}
		}
	}

	public void addMenuItem(String name, String desc, int price, int type)
	{
		ArrayList<MenuGroup> groups = menu.getGroups();
		for(int i = 0; i < groups.size(); i++)
		{
			if(groups.get(i).getName() == "pizza")
			{
				if(type == 0)
				{
					groups.get(i).addItem(new MenuItem(name, desc, price));
				}
			}
			if(groups.get(i).getName() == "soda")
			{
				if(type == 1)
				{
					groups.get(i).addItem(new MenuItem(name, desc, price));
				}
			}
			if(groups.get(i).getName() == "other")
			{
				if(type == 2)
				{
					groups.get(i).addItem(new MenuItem(name, desc, price));
				}
			}
		}
	}
}
