/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

public class MenuItem
{

	private String name;
	private String desc;
	private boolean isSpecial;
	private int price;
	private int discount = 20;

	/**
	 * Constructor for a menu item
	 *
	 * @param name
	 * @param desc
	 * @param price
	 */
	public MenuItem(String name, String desc, int price)
	{
		this.name = name;
		this.desc = desc;
		this.isSpecial = false;
		this.price = price;
	}

	/**
	 * set the price of a menu item
	 *
	 * @param price
	 */
	public void setPrice(int price)
	{
		this.price = price;
	}

	/**
	 * set the description of a menu item
	 *
	 * @param desc
	 */
	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public String getDesc()
	{
		return desc;
	}

	/**
	 * set the name of a menu item
	 *
	 * @param name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	/**
	 * get the total price of an item including toppings
	 *
	 * @return
	 */
	public int returnTotal()
	{
		if(this.isSpecial)
		{
			return this.price * (100 - discount) / 100;
		}
		return this.price;
	}

	public void setSpecial()
	{
		this.isSpecial = true;
	}

	public boolean isSpecial()
	{
		return this.isSpecial;
	}

	public void notSpecial()
	{
		this.isSpecial = false;
	}

	public String toString()
	{
		if(this.isSpecial)
		{
			return name + " " + " $" + (float) (price * (100 - discount)) / 10000.0;
		}
		return name + " " + " $" + (float) price / 100.0;
	}
}
