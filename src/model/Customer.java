package model;

public class Customer extends Person
{
	public Customer(String name, String address, String city, String state,
	                String zipCode, String phoneNumber, int customerID)
	{
		super(name, address, city, state, zipCode, phoneNumber);
	}

	public String toString()
	{
		return this.getName();
	}
}
