/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

public class Manager extends Employee
{

	/**
	 * Constructor for a manager
	 *
	 * @param name
	 * @param address
	 * @param city
	 * @param state
	 * @param zipCode
	 * @param phoneNumber
	 * @param employeeID
	 * @param loginID
	 * @param password
	 */
	public Manager(String name, String address, String city, String state,
	               String zipCode, String phoneNumber, String loginID, String password)
	{
		super(name, address, city, state, zipCode, phoneNumber, loginID, password);
	}

	public String toString()
	{
		return this.getName();
	}
}
