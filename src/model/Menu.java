/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

import java.util.ArrayList;

public class Menu
{

	private ArrayList<MenuGroup> groups;

	/** Constructor for a new menu */
	public Menu()
	{
		this.groups = new ArrayList<MenuGroup>();
	}

	/**
	 * add a new menu group
	 *
	 * @param group
	 */
	public boolean addGroup(MenuGroup group)
	{
		if(!groups.contains(group))
		{
			getGroups().add(group);
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * remove a menu group
	 *
	 * @param group
	 *
	 * @return
	 */
	public boolean removeGroup(MenuGroup group)
	{
		if(getGroups().contains(group))
		{
			getGroups().remove(group);
			return true;
		}
		else
		{
			return false;
		}
	}

	public void clearSpecials()
	{
		for(int i = 0; i < this.groups.size(); i++)
		{
			this.groups.get(i).clearSpecials();
		}
	}

	/** @return the groups */
	public ArrayList<MenuGroup> getGroups()
	{
		return groups;
	}
}
