/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

public class OrderItem
{

	private int quantity;
	private MenuItem item;
	private int lineCost;

	/**
	 * Constructor for an order item
	 *
	 * @param quantity
	 * @param item
	 */
	public OrderItem(int quantity, MenuItem item)
	{
		this.quantity = quantity;
		this.item = item;
		calculateLineCost();
	}

	/** calculate the cost for an order line */
	private void calculateLineCost()
	{
		lineCost = getQuantity() * (getItem().returnTotal());
	}

	/**
	 * get the quantity of an item in an order line
	 *
	 * @return the quantity
	 */
	public int getQuantity()
	{
		return quantity;
	}

	/**
	 * get an order item
	 *
	 * @return the item
	 */
	public MenuItem getItem()
	{
		return item;
	}

	/**
	 * get the line cost
	 *
	 * @return the lineCost
	 */
	public int getLineCost()
	{
		return lineCost;
	}

	public String toString()
	{
		return +quantity + " " + item.getName() + " : " + (float) lineCost / 100.0;
	}
}
