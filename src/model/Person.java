/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

/*
 * TODO
 * things to possibly take into consideration:
 * 	- consider adding regex to set method to not allow any extraneous characters not needed in the variable fields
 *      - if regex added to set methods, should probably use those methods in the constructor
 *      - time permitting of course
 */
package model;

public class Person
{

	private String name;
	private String address;
	private String city;
	private String state;
	private String zipCode;
	private String phoneNumber;

	/**
	 * Constructor for a Person
	 *
	 * @param name
	 * 		of the Person
	 * @param address
	 * 		of the Person
	 * @param city
	 * 		of the Person
	 * @param state
	 * 		of the Person
	 * @param zipCode
	 * 		of the Person
	 * @param phoneNumber
	 * 		of the Person
	 */
	public Person(String name, String address, String city, String state, String zipCode, String phoneNumber)
	{
		this.name = name;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.phoneNumber = phoneNumber;
	}

	/**
	 * gets a persons name
	 *
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * sets a persons name
	 *
	 * @param name
	 * 		the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * gets a persons address
	 *
	 * @return the address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * sets a persons address
	 *
	 * @param address
	 * 		the address to set
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}

	/**
	 * gets a persons city
	 *
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * sets a persons city
	 *
	 * @param city
	 * 		the city to set
	 */
	public void setCity(String city)
	{
		this.city = city;
	}

	/**
	 * gets a persons state of residence
	 *
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * sets a persons state of residence
	 *
	 * @param state
	 * 		the state to set
	 */
	public void setState(String state)
	{
		this.state = state;
	}

	/**
	 * gets a persons zip code
	 *
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * gets a persons zip code
	 *
	 * @param zipCode
	 * 		the zipCode to set
	 */
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	/**
	 * gets a persons phone number
	 *
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * sets a persons phone number
	 *
	 * @param phoneNumber
	 * 		the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/** Allows person to place a new order */
	public void placeOrder()
	{
		//TODO
	}

	/**
	 * allows a Person to view a specific order
	 *
	 * @param orderNumber
	 * 		order number to be viewed
	 *
	 * @return
	 */
	public Order viewOrder(int orderNum)
	{
		//TODO get order by number
		return new Order((Customer) this, orderNum);
	}

	/** Override of equals method */
	@Override
	public boolean equals(Object that)
	{
		if(this == that)
		{
			return true;
		}
		if(!(that.getClass() == this.getClass()))
		{
			return false;
		}
		else
		{
			return this.getName().equals(((Person) that).getName()) &&
			       this.getAddress().equals(((Person) that).getAddress()) &&
			       this.getCity().equals(((Person) that).getCity()) &&
			       this.getState().equals(((Person) that).getState()) &&
			       this.getZipCode().equals(((Person) that).getZipCode()) &&
			       this.getPhoneNumber().equals(((Person) that).getPhoneNumber());
		}
	}

	/** Override of toString method */
	@Override
	public String toString()
	{
		String person;
		person = "Name: " + this.name + "\n" +
		         "Address: " + this.address + "\n" +
		         "City: " + this.city + "\n" +
		         "State: " + this.state + "\n" +
		         "Zip Code: " + this.zipCode + "\n" +
		         "Phone Number: " + this.phoneNumber + "\n";
		return person;
	}
}
