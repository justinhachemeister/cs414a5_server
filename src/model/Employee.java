/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

public class Employee extends Person
{

	private String loginID;
	private String password;  //TODO irl should store hash instead of password
	private boolean manager;
	private boolean chef;
	private boolean cashier;

	/**
	 * Constructor for an employee
	 *
	 * @param name
	 * @param address
	 * @param city
	 * @param state
	 * @param zipCode
	 * @param phoneNumber
	 * @param employeeID
	 * @param loginID
	 * @param password
	 */
	public Employee(String name, String address, String city, String state,
	                String zipCode, String phoneNumber, String loginID, String password)
	{
		super(name, address, city, state, zipCode, phoneNumber);
		this.loginID = loginID;
		this.password = password;
		this.cashier = true;
		this.manager = false;
		this.chef = false;
	}

	/** @return the manager */
	public boolean isManager()
	{
		return manager;
	}

	/**
	 * @param manager
	 * 		the manager to set
	 */
	public void setManager()
	{
		this.cashier = false;
		this.chef = false;
		this.manager = true;
	}

	/** @return the chef */
	public boolean isChef()
	{
		return chef;
	}

	/**
	 * @param chef
	 * 		the chef to set
	 */
	public void setChef()
	{
		this.cashier = false;
		this.chef = true;
		this.manager = false;
	}

	/** @return the chef */
	public boolean isCashier()
	{
		return cashier;
	}

	/**
	 * @param chef
	 * 		the chef to set
	 */
	public void setCashier()
	{
		this.cashier = true;
		this.chef = false;
		this.manager = false;
	}

	/**
	 * gets an employees login ID
	 *
	 * @return the loginID
	 */
	public String getLoginID()
	{
		return loginID;
	}

	/**
	 * sets an employees login ID
	 *
	 * @param loginID
	 * 		the loginID to set
	 */
	public void setLoginID(String loginID)
	{
		//TODO check to make sure the userID isn't already being used by another employee
		this.loginID = loginID;
	}

	/**
	 * get an employees password
	 *
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * set an employee password
	 *
	 * @param password
	 * 		the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	public String toString()
	{
		return this.getName();
	}
}
