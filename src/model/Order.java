/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

import java.util.ArrayList;

public class Order
{

	private Customer madefor;
	private ArrayList<OrderItem> orderItems;
	private Payment payment;
	private int status; //use to set order progress
	private int orderNum;
	private int subTotal;
	private int total;
	private int taxPercent = 0;

	/**
	 * Constructor for a new customer
	 *
	 * @param orderCust
	 */
	public Order(Customer orderCust, int orderNum)
	{
		this.setMadefor(orderCust);
		this.orderItems = new ArrayList<OrderItem>();
		this.status = 0;
		this.orderNum = orderNum;
		this.subTotal = 0;
		this.total = 0;
	}

	/**
	 *
	 * @param orderCust
	 * @param orderItems
	 */
	/*public Order(Customer orderCust, ArrayList<MenuItem> orderItems)
		 {
			  this.madefor = orderCust;
			  this.orderItems = orderItems;
		 }*/

	/**
	 * add an item to an order
	 *
	 * @param orderItem
	 */
	public void addItem(OrderItem orderItem)
	{
		this.orderItems.add(orderItem);
		subTotal += orderItem.getLineCost();
		total = calculateTotal();
	}

	/**
	 * remove an item from an order
	 *
	 * @param removeItem
	 *
	 * @return
	 */
	public boolean removeItem(OrderItem removeItem)
	{
		if(this.orderItems.contains(removeItem))
		{
			this.orderItems.remove(removeItem);
			subTotal -= removeItem.getLineCost();
			total = calculateTotal();
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * set an order status
	 * 0 = ordered
	 * 1 = paid
	 * 2 = in progress
	 * 3 = complete
	 *
	 * @param status
	 */
	public boolean setStatus(int status)
	{
		if(status >= 0 && status < 4)
		{
			this.status = status;
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * get an order status
	 *
	 * @return
	 */
	public int getStatus()
	{
		return this.status;
	}

	/**
	 * get the total cost for an order
	 *
	 * @return
	 */
	public int getTotal()
	{
		return total;
	}

	/**
	 * make a cash payment
	 *
	 * @param amount
	 *
	 * @return
	 */
	public boolean makePayment(int amount)
	{
		payment = new Payment(amount);
		return true;
	}

	/**
	 * make a cc payment
	 *
	 * @param ccType
	 * @param ccNumber
	 *
	 * @return
	 */
	public boolean makePayment(String ccType, int ccNumber)
	{
		payment = new Payment(ccType, ccNumber, getTotal());
		return true;
	}

	/**
	 * calculate the change due to a customer
	 *
	 * @param payment
	 *
	 * @return
	 */
	public int calculateChange(Payment payment)
	{
		return payment.getAmount() - getTotal();
	}

	/**
	 * recalculate the order total when adding or deleting an item
	 *
	 * @return
	 */
	private int calculateTotal()
	{
		return (int) (subTotal + ((taxPercent / 100.0) * subTotal));
	}

	/** @return the payment */
	public Payment getPayment()
	{
		return payment;
	}

	public String toString()
	{
		return orderNum + " " + madefor.getName() + " $" + (float) total / 100.0;
	}

	public Customer getMadefor()
	{
		return madefor;
	}

	public void setMadefor(Customer madefor)
	{
		this.madefor = madefor;
	}
}
