/*
 * Jon McQuillan
 * Robert Melrose
 * Eric Steinert
 * 
 * CS414 A4 Pizza Shop POS System
 */

package model;

import java.util.ArrayList;

public class PizzaShopGUIHandler
{

	//private PizzaShopGUI gui;
	PizzaShop shop;
	private Order currentOrder;
	private ArrayList<OrderItem> currentOrderItems;

	public PizzaShopGUIHandler()
	{
		//this.gui = newGUI;
		this.shop = new PizzaShop();
		this.currentOrderItems = new ArrayList<OrderItem>();
	}

	/**
	 * *************************
	 * Methods Need
	 * get Pizza List as String[]
	 * get Soda List as String[]
	 * get Item List as String[]
	 * get Topping List as String []
	 * get Order List as String[]
	 * get entire menu as a single String[]
	 * remove menu item(int location) - location in the entire menu String[]
	 * remove topping(int location) - location in topping String[]
	 * remove order(int location) - location in order String[]
	 * make special(int location) - location in the entire menu String[]
	 * remove special()
	 * add order(string address, string zip, string phone, int pizzaOrderLocation[], int sodaOrderLocation[], int itemOrderLocation[])
	 * add topping(string name, int price)
	 * add item(string name, string desc, int basePrice, int toppingLocation[])
	 * mark order finished(int location) in orderList
	 * <p/>
	 * Decide how we want to handle empolyees and their login. Return a list of employees to login as? Or just have the GUI pass down a value indicating that a Cashier, Manager, Chef has logged in.
	 * <p/>
	 * ****************************
	 */

	public String[] getOrdersList()
	{
		ArrayList<Order> pendingOrders = shop.viewOrders();
		String[] temp = new String[ pendingOrders.size() ];
		for(int i = 0; i < pendingOrders.size(); i++)
		{
			if(pendingOrders.get(i).getStatus() != 3)
			{
				temp[ i ] = pendingOrders.get(i).toString();
			}
			else
			{
				temp[ i ] = "Finished: " + pendingOrders.get(i).toString();
			}
		}
		return temp;
	}

	public String[] getPizzaList()
	{
		//shop.getMenu()....
		MenuGroup pizzasMenuGroup = shop.getPizzas();
		String[] Pizzas = new String[ pizzasMenuGroup.getItems().size() ];
		for(int i = 0; i < pizzasMenuGroup.getItems().size(); i++)
		{
			Pizzas[ i ] = pizzasMenuGroup.getItems().get(i).toString();
		}
		return Pizzas;
	}

	public String[] getSodaList()
	{
		//shop.getMenu()....
		MenuGroup sodasMenuGroup = shop.getSodas();
		String[] Sodas = new String[ sodasMenuGroup.getItems().size() ];
		for(int i = 0; i < sodasMenuGroup.getItems().size(); i++)
		{
			Sodas[ i ] = sodasMenuGroup.getItems().get(i).toString();
		}
		return Sodas;
	}

	public String[] getOtherList()
	{
		//shop.getMenu()....
		MenuGroup othersMenuGroup = shop.getOthers();
		String[] Sodas = new String[ othersMenuGroup.getItems().size() ];
		for(int i = 0; i < othersMenuGroup.getItems().size(); i++)
		{
			Sodas[ i ] = othersMenuGroup.getItems().get(i).toString();
		}
		return Sodas;
	}

	public void RemovePizza(int index)
	{
		shop.RemovePizza(index);
	}

	public void RemoveSoda(int index)
	{
		shop.RemoveSoda(index);
	}

	public void RemoveOther(int index)
	{
		shop.RemoveOther(index);
	}

	public String CompleteOrder(String name, String address, String city, String state, String zipCode, String phoneNumber, int paidAmount)
	{
		Customer currentCustomer = new Customer(name, address, city, state, zipCode, phoneNumber, shop.getNextCustomerID());
		currentOrder = new Order(currentCustomer, shop.getNextOrderNum());
		for(int i = 0; i < currentOrderItems.size(); i++)
		{
			currentOrder.addItem(currentOrderItems.get(i));
		}
		shop.addOrder(currentOrder);
		if(paidAmount == 0)
		{
			return "Credit Approved";
		}
		Integer change = paidAmount - currentOrder.getTotal();
		ClearOrder();
		return change.toString();
	}

	public void ClearOrder()
	{
		currentOrderItems.clear();
	}

	public int GetTotal()
	{
		int total = 0;
		for(int i = 0; i < currentOrderItems.size(); i++)
		{
			total += currentOrderItems.get(i).getLineCost();
		}
		return total;
	}

	public void addPizzaItem(int arrayLocation, int quantity)
	{
		currentOrderItems.add(new OrderItem(quantity, shop.getPizzas().getItems().get(arrayLocation)));
	}

	public void addSodaItem(int arrayLocation, int quantity)
	{
		currentOrderItems.add(new OrderItem(quantity, shop.getSodas().getItems().get(arrayLocation)));
	}

	public void addOtherItem(int arrayLocation, int quantity)
	{
		currentOrderItems.add(new OrderItem(quantity, shop.getOthers().getItems().get(arrayLocation)));
	}

	public String SelectedItems()
	{
		String outPut = "";
		for(int i = 0; i < currentOrderItems.size(); i++)
		{
			outPut = outPut + currentOrderItems.get(i).toString() + "\n";
		}
		return outPut;
	}

	public void setPizzaSpecial(int arrayLocation)
	{
		shop.setPizzaSpecial(arrayLocation);
	}

	public void setDrinkSpecial(int arrayLocation)
	{
		shop.setDrinkSpecial(arrayLocation);
	}

	public void setOtherSpecial(int arrayLocation)
	{
		shop.setOtherSpecial(arrayLocation);
	}

	public void clearSpecials()
	{
		shop.clearSpecials();
	}

	public void addItem(String name, String desc, int price, int type)
	{
		shop.addMenuItem(name, desc, price, type);
	}

	public int employeeType(int arrayLocation)
	{
		Employee employee = shop.getEmployeeList().get(arrayLocation);
		if(employee.isCashier())
		{
			return 0;
		}
		if(employee.isChef())
		{
			return 1;
		}
		return 2;
	}

	public void addEmployee(String name, String address, String city, String state, String zipcode, String phone, String login, String password, int type)
	{
		shop.addEmployee(name, address, city, state, zipcode, phone, login, password);
	}

	public ArrayList<String> getEmployees()
	{
		ArrayList<Employee> employees = shop.getEmployeeList();
		ArrayList<String> employeeNames = new ArrayList<String>();
		for(int i = 0; i < employees.size(); i++)
		{
			employeeNames.add(employees.get(i).toString());
		}
		return employeeNames;
	}

	public void markOrderFinished(int arrayLocation)
	{
		shop.finishOrder(arrayLocation);
	}

	public void removeOrder(int arrayLocation)
	{
		shop.removeOrder(arrayLocation);
	}
}
