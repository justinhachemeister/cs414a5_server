package model;

public class Chef extends Employee
{
	public Chef(String name, String address, String city, String state,
	            String zipCode, String phoneNumber, int employeeID, String loginID, String password)
	{
		super(name, address, city, state, zipCode, phoneNumber, loginID, password);
	}

	public String toString()
	{
		return this.getName();
	}
}
