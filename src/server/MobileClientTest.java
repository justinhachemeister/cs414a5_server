package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;

public class MobileClientTest extends Thread
{
	private Selector selector;
	private SocketChannel channel;
	private ByteBuffer reader = ByteBuffer.allocate(8192);
	private List<ChangeRequest> opChanges = new LinkedList<ChangeRequest>();
	private Map<SocketChannel, List<ByteBuffer>> outData = new HashMap<SocketChannel, List<ByteBuffer>>();


	public class ChangeRequest
	{
		public static final int REGISTER = 1;
		public static final int CHANGE_OPS = 2;
		public SocketChannel socketChannel;
		public int changeType;
		public int skOps;

		public ChangeRequest(SocketChannel socketChannel, int changeType, int selectionKeyOps)
		{
			this.socketChannel = socketChannel;
			this.changeType = changeType;
			this.skOps = selectionKeyOps;
		}
	}

	public MobileClientTest(String remote_hostname, int remote_portnumber) throws IOException
	{
		this.selector = Selector.open();
		this.connect(remote_hostname, remote_portnumber);
	}

	private void connect(String remote_hostname, int remote_portnumber) throws IOException
	{
		this.channel = SocketChannel.open();
		this.channel.configureBlocking(false);
		this.channel.connect(new InetSocketAddress(InetAddress.getByName(remote_hostname), remote_portnumber));
		synchronized(this.opChanges)
		{
			this.opChanges.add(new ChangeRequest(this.channel, ChangeRequest.REGISTER, SelectionKey.OP_CONNECT));
		}
	}

	private void changeOps()
	{
		synchronized(this.opChanges)
		{
			Iterator<ChangeRequest> it = this.opChanges.iterator();
			while(it.hasNext())
			{
				ChangeRequest request = it.next();
				it.remove();
				switch(request.changeType)
				{
					case ChangeRequest.REGISTER:
						try
						{
							request.socketChannel.register(this.selector, request.skOps);
						}
						catch(ClosedChannelException e)
						{
							System.err.println("changeOps():\n" + e.toString());
						}
						break;
					case ChangeRequest.CHANGE_OPS:
						SelectionKey key = request.socketChannel.keyFor(this.selector);
						key.interestOps(request.skOps);
						break;
				}
			}
		}
	}

	@Override
	public void run()
	{
		while(!this.isInterrupted())
		{
			this.changeOps();
			try
			{
				this.selector.select();
				Iterator<SelectionKey> keys = this.selector.selectedKeys().iterator();
				while(keys.hasNext())
				{
					SelectionKey key = keys.next();
					keys.remove();
					if(key.isValid())
					{
						if(key.isConnectable())
						{
							this.finish(key);
						}
						else if(key.isReadable())
						{
							this.read(key);
						}
						else if(key.isWritable())
						{
							this.write(key);
						}
					}
				}
			}
			catch(IOException e)
			{
				System.err.println("run():\n" + e.toString());
			}
		}
	}

	@SuppressWarnings( { "rawtypes", "unchecked" })
	public void send(byte[] data)
	{
		System.out.println("Sending a message to the server.");
		synchronized(this.outData)
		{
			List<ByteBuffer> queue = (List) this.outData.get(this.channel);
			if(queue == null)
			{
				queue = new ArrayList<ByteBuffer>();
				this.outData.put(this.channel, queue);
			}
			queue.add(ByteBuffer.wrap(data));
		}
		this.selector.wakeup();
	}

	private void finish(SelectionKey key)
	{
		SocketChannel channel = (SocketChannel) key.channel();
		try
		{
			channel.finishConnect();
		}
		catch(IOException e)
		{
			System.err.println("finish():\n" + e.toString());
			key.cancel();
			return;
		}
		key.interestOps(SelectionKey.OP_WRITE);
	}

	private void read(SelectionKey key) throws IOException
	{
		System.out.println("Reading a response from the server.");
		SocketChannel channel = (SocketChannel) key.channel();
		this.reader.clear();
		int read;
		try
		{
			read = channel.read(this.reader);
		}
		catch(IOException e)
		{
			key.cancel();
			channel.close();
			return;
		}
		if(read == -1)
		{
			key.channel().close();
			key.cancel();
			return;
		}
		this.process(new String(this.reader.array()));
	}

	@SuppressWarnings("rawtypes")
	private void write(SelectionKey key) throws IOException
	{
		System.out.println("Writing the request to the server.");
		SocketChannel channel = (SocketChannel) key.channel();
		synchronized(this.outData)
		{
			List queue = (List) this.outData.get(channel);
			while(queue != null && !queue.isEmpty())
			{
				ByteBuffer writer = (ByteBuffer) queue.get(0);
				channel.write(writer);
				if(writer.remaining() > 0)
				{
					break;
				}
				queue.remove(0);
			}
			if(queue != null && queue.isEmpty())
			{
				key.interestOps(SelectionKey.OP_READ);
			}
		}
	}

	private void process(String response)
	{
		System.out.println("Processing the response from the server.");
		System.out.println(response);
	}

	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			System.err.println("USAGE: ./MobileServer remote_hostname remote_portnumber");
			return;
		}
		int remote_portnumber = Integer.parseInt(args[ 1 ]);
		if(remote_portnumber <= 1023 && remote_portnumber >= 65535)
		{
			System.err.println("Invalid port number. Must be an integer between 1024 and 65535.");
			return;
		}
		try
		{
			MobileClientTest client = new MobileClientTest(args[ 0 ], remote_portnumber);
			Thread t = new Thread(client);
			t.start();

			//This caused the process() method in MobileServer to be passed:
			//	"REQUEST_PIZZA_MENUREQUEST_SODA_MENUREQUEST_TOTAL1|2|REQUEST_ADD_ITEMREQUEST_ORDER_CONTENTS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 "

			System.out.println("REQUEST_PIZZA_MENU");
			client.send(new String("REQUEST_PIZZA_MENU").getBytes());

			System.out.println("REQUEST_SODA_MENU");
			client.send(new String("REQUEST_SODA_MENU").getBytes());

			System.out.println("REQUEST_TOTAL");
			client.send(new String("REQUEST_TOTAL").getBytes());

			System.out.println("1|2|REQUEST_ADD_ITEM");
			client.send(new String("1|2|REQUEST_ADD_ITEM").getBytes());

			System.out.println("REQUEST_ORDER_CONTENTS");
			client.send(new String("REQUEST_ORDER_CONTENTS").getBytes());

			System.out.println("Test Succeded!");
		}
		catch(IOException e)
		{
			System.err.println("main(): Error setting up mobile server test.\n" + e.toString());
		}
	}
}
