package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;

public class MobileServer extends Thread
{
	private Selector selector;
	private ByteBuffer reader = ByteBuffer.allocate(8192);
	private List<ChangeRequest> opChanges = new LinkedList<ChangeRequest>();
	private Map<SocketChannel, List<ByteBuffer>> outData = new HashMap<SocketChannel, List<ByteBuffer>>();


	public class ChangeRequest
	{
		public static final int REGISTER = 1;
		public static final int CHANGE_OPS = 2;
		public SocketChannel socketChannel;
		public int changeType;
		public int skOps;

		public ChangeRequest(SocketChannel socketChannel, int changeType, int selectionKeyOps)
		{
			this.socketChannel = socketChannel;
			this.changeType = changeType;
			this.skOps = selectionKeyOps;
		}
	}

	public MobileServer(int portnumber) throws IOException
	{
		try
		{
			System.out.println("Mobile Server Up On: " + InetAddress.getLocalHost().getHostAddress() + ":" + portnumber);
		}
		catch(UnknownHostException e)
		{
			System.err.println(e.toString());
		}
		this.selector = Selector.open();
		this.listen(portnumber);
	}

	private void listen(int port) throws IOException
	{
		ServerSocketChannel channel = ServerSocketChannel.open();
		channel.configureBlocking(false);
		channel.socket().bind(new InetSocketAddress(port));
		channel.register(this.selector, SelectionKey.OP_ACCEPT);
	}

	private void changeOps()
	{
		synchronized(this.opChanges)
		{
			Iterator<ChangeRequest> it = this.opChanges.iterator();
			while(it.hasNext())
			{
				ChangeRequest request = it.next();
				it.remove();
				switch(request.changeType)
				{
					case ChangeRequest.REGISTER:
						try
						{
							request.socketChannel.register(this.selector, request.skOps);
						}
						catch(ClosedChannelException e)
						{
							System.err.println("changeOps():\n" + e.toString());
						}
						break;
					case ChangeRequest.CHANGE_OPS:
						SelectionKey key = request.socketChannel.keyFor(this.selector);
						key.interestOps(request.skOps);
						break;
				}
			}
		}
	}

	@Override
	public void run()
	{
		while(!this.isInterrupted())
		{
			this.changeOps();
			try
			{
				this.selector.select();
				Iterator<SelectionKey> keys = this.selector.selectedKeys().iterator();
				while(keys.hasNext())
				{
					SelectionKey key = keys.next();
					keys.remove();
					if(key.isValid())
					{
						if(key.isAcceptable())
						{
							this.accept(key);
						}
						else if(key.isReadable())
						{
							this.read(key);
						}
						else if(key.isWritable())
						{
							this.write(key);
						}
					}
				}
			}
			catch(IOException e)
			{
				System.out.println("run():\n" + e.toString());
			}
		}
	}

	private void accept(SelectionKey key) throws IOException
	{
		System.out.println("Accepting an incoming connection!");
		SocketChannel channel = ((ServerSocketChannel) key.channel()).accept();
		channel.configureBlocking(false);
		channel.register(this.selector, SelectionKey.OP_READ);
	}

	private void read(SelectionKey key) throws IOException
	{
		System.out.println("Reading data from a client!");
		SocketChannel channel = (SocketChannel) key.channel();
		this.reader.clear();
		int read;
		try
		{
			read = channel.read(this.reader);
		}
		catch(IOException e)
		{
			key.cancel();
			channel.close();
			return;
		}
		if(read == -1)
		{
			key.channel().close();
			key.cancel();
			return;
		}
		this.process(channel, new String(this.reader.array()));
	}

	@SuppressWarnings( { "rawtypes", "unchecked" })
	private void write(SelectionKey key) throws IOException
	{
		SocketChannel channel = (SocketChannel) key.channel();
		synchronized(this.outData)
		{
			List<ByteBuffer> queue = (List) this.outData.get(channel);
			while(!queue.isEmpty())
			{
				ByteBuffer writer = (ByteBuffer) queue.get(0);
				channel.write(writer);
				if(writer.remaining() > 0)
				{
					break;
				}
				queue.remove(0);
			}
			if(queue.isEmpty())
			{
				key.interestOps(SelectionKey.OP_READ);
			}
		}
	}

	@SuppressWarnings( { "unchecked", "rawtypes" })
	public void process(SocketChannel channel, String request)
	{
		System.out.println("Processing Request \"" + request + "\"");

		StringBuilder out = new StringBuilder();
		if(request.contains("REQUEST_PIZZA_MENU"))
		{
			//out.append(PizzaStoreImpl.model.getPizzaList().length);
			for(String s : PizzaStoreImpl.model.getPizzaList())
			{
				out.append(s + "|");
			}
		}
		else if(request.contains("REQUEST_SODA_MENU"))
		{
			for(String s : PizzaStoreImpl.model.getSodaList())
			{
				out.append(s + "|");
			}
		}
		else if(request.contains("REQUEST_OTHER_MENU"))
		{
			for(String s : PizzaStoreImpl.model.getOrdersList())
			{
				out.append(s + "|");
			}
		}
		else if(request.contains("REQUEST_TOTAL"))
		{
			out.append(PizzaStoreImpl.model.GetTotal());
		}
		else if(request.contains("REQUEST_ADD_ITEM"))
		{
			String[] add_item = request.split("|");
			switch(Integer.parseInt(add_item[ 1 ]))
			{
				case 0:
					PizzaStoreImpl.model.addPizzaItem(Integer.parseInt(add_item[ 3 ]), 1);
					break;
				case 1:
					PizzaStoreImpl.model.addSodaItem(Integer.parseInt(add_item[ 3 ]), 1);
					break;
				case 2:
					PizzaStoreImpl.model.addOtherItem(Integer.parseInt(add_item[ 3 ]), 1);
					break;
			}
			return;
		}
		else if(request.contains("REQUEST_ORDER_CONTENTS"))
		{
			out.append(PizzaStoreImpl.model.SelectedItems());
		}
		synchronized(this.opChanges)
		{
			this.opChanges.add(new ChangeRequest(channel, ChangeRequest.CHANGE_OPS, SelectionKey.OP_WRITE));
			synchronized(this.outData)
			{
				List queue = (List) this.outData.get(channel);
				if(queue == null)
				{
					queue = new ArrayList<ByteBuffer>();
					this.outData.put(channel, queue);
				}
				queue.add(ByteBuffer.wrap(out.toString().getBytes()));
			}
		}
		this.selector.wakeup();
	}

	public static void main(String[] args)
	{
		if(args.length == 0 || args.length > 1)
		{
			System.err.println("USAGE: ./MobileServer portnumber");
			return;
		}
		int portnumber = Integer.parseInt(args[ 0 ]);
		if(portnumber <= 1023 && portnumber >= 65535)
		{
			System.err.println("Invalid port number. Must be an integer between 1024 and 65535.");
			return;
		}
		try
		{
			MobileServer server = new MobileServer(portnumber);
			Thread t = new Thread(server);
			t.start();
		}
		catch(IOException e)
		{
			System.err.println("main(): Error setting up mobile server.\n" + e.toString());
		}
	}
}