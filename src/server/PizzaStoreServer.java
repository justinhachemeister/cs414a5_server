package server;

import java.io.IOException;
import java.rmi.Naming;

public class PizzaStoreServer
{
	public PizzaStoreServer(String rmiHost, String rmiPort)
	{
		//RMI
		String rmiUrl = "rmi://" + rmiHost + ":" + rmiPort + "/PizzaStore";
		try
		{
			PizzaStore p = new PizzaStoreImpl();
			Naming.rebind(rmiUrl, p);
			System.out.println("RMI Server Started :" + rmiUrl);
		}
		catch(Exception e)
		{
			System.out.println("Trouble: " + e);
		}
	}

	public static void main(String args[])
	{
		int portnumber = Integer.parseInt(args[ 1 ]);
		if(portnumber <= 1023 && portnumber >= 65535)
		{
			System.err.println("Invalid port number. Must be an integer between 1024 and 65535.");
			return;
		}
		try
		{
			MobileServer server = new MobileServer(portnumber + 3);
			Thread t = new Thread(server);
			t.start();
		}
		catch(IOException e)
		{
			System.err.println("main(): Error setting up mobile server.\n" + e.toString());
		}
		new PizzaStoreServer(args[ 0 ], args[ 1 ]);
	}
}
