package server;

import model.PizzaShopGUIHandler;

import java.util.ArrayList;


public class PizzaStoreImpl extends java.rmi.server.UnicastRemoteObject implements PizzaStore
{

	private static final long serialVersionUID = 1L;
	protected static PizzaShopGUIHandler model;

	public PizzaStoreImpl()
			throws java.rmi.RemoteException
	{
		super();
		model = new PizzaShopGUIHandler();
	}

	public String[] getOrdersList()
	{
		return model.getOrdersList();
	}

	public String[] getPizzaList()
	{
		return model.getPizzaList();
	}

	public String[] getSodaList()
	{
		return model.getSodaList();
	}

	public String[] getOtherList()
	{
		return model.getOtherList();
	}

	public void RemovePizza(int index)
	{
		model.RemovePizza(index);
	}

	public void RemoveSoda(int index)
	{
		model.RemoveSoda(index);
	}

	public void RemoveOther(int index)
	{
		model.RemoveOther(index);
	}

	public String CompleteOrder(String name, String address, String city, String state, String zipCode, String phoneNumber, int paidAmount)
	{
		return model.CompleteOrder(name, address, city, state, zipCode, phoneNumber, paidAmount);
	}

	public void ClearOrder()
	{
		model.ClearOrder();
	}

	public int GetTotal()
	{
		return model.GetTotal();
	}

	public void addPizzaItem(int arrayLocation, int quantity)
	{
		model.addPizzaItem(arrayLocation, quantity);
	}

	public void addSodaItem(int arrayLocation, int quantity)
	{
		model.addSodaItem(arrayLocation, quantity);
	}

	public void addOtherItem(int arrayLocation, int quantity)
	{
		model.addOtherItem(arrayLocation, quantity);
	}

	public String SelectedItems()
	{
		return model.SelectedItems();
	}

	public void setPizzaSpecial(int arrayLocation)
	{
		model.setPizzaSpecial(arrayLocation);
	}

	public void setDrinkSpecial(int arrayLocation)
	{
		model.setDrinkSpecial(arrayLocation);
	}

	public void setOtherSpecial(int arrayLocation)
	{
		model.setOtherSpecial(arrayLocation);
	}

	public void clearSpecials()
	{
		model.clearSpecials();
	}

	public void addItem(String name, String desc, int price, int type)
	{
		model.addItem(name, desc, price, type);
	}

	public int employeeType(int arrayLocation)
	{
		return model.employeeType(arrayLocation);
	}

	public void addEmployee(String name, String address, String city, String state, String zipcode, String phone, String login, String password, int type)
	{
		model.addEmployee(name, address, city, state, zipcode, phone, login, password, type);
	}

	public ArrayList<String> getEmployees()
	{
		return model.getEmployees();
	}

	public void markOrderFinished(int arrayLocation)
	{
		model.markOrderFinished(arrayLocation);
	}

	public void removeOrder(int arrayLocation)
	{
		model.removeOrder(arrayLocation);
	}
}
