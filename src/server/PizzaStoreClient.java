package server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PizzaStoreClient
{
	private JFrame frmPizzaOrderingSystem;
	private JTextField nameField;
	private JTextField descField;
	private JTextField priceField;
	private JTextField totalField;
	private JList pizzaList;
	private JList drinkList;
	private JList otherList;
	private JList orderList;
	private JTextArea orderContents;
	private JTabbedPane tabbedPane;
	private JScrollPane pizzaPane;
	private JScrollPane drinkPane;
	private JScrollPane otherPane;
	private JScrollPane currentOrderPane;
	private JScrollPane editDrinkPane;
	private JComboBox loginBox;
	private JComboBox typeBox;
	private JScrollPane editPizzaPane;
	private JScrollPane editOtherPane;
	private JList editPizzaList;
	private JList editDrinkList;
	private JList editOtherList;
	private JTextField payField;
	private JTextField quantityField;
	private JTextField phoneField;
	private JTextField addressField;
	private JTextField custName;

	static PizzaStore model;

	public PizzaStoreClient() throws RemoteException
	{
		initialize();
	}

	public static void main(String[] args) throws RemoteException
	{
		//RMI
		String hostname = args[ 0 ];
		String port = args[ 1 ];
		String rmiName = "rmi://" + hostname + ":" + port + "/PizzaStore";
		System.out.println("Connecting to: " + rmiName);

		try
		{
			//Connect to Server
			model = (PizzaStore) Naming.lookup(rmiName);
			System.out.println("Pizza Store Client Connected to Pizza Store Server");
		}
		catch(MalformedURLException murle)
		{
			System.out.println();
			System.out.println(
					"MalformedURLException");
			System.out.println(murle);
		}
		catch(RemoteException re)
		{
			System.out.println();
			System.out.println(
					"RemoteException");
			System.out.println(re);
		}
		catch(NotBoundException nbe)
		{
			System.out.println();
			System.out.println(
					"NotBoundException");
			System.out.println(nbe);
		}
		catch(
				ArithmeticException
						ae)
		{
			System.out.println();
			System.out.println(
					"java.lang.ArithmeticException");
			System.out.println(ae);
		}

		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					PizzaStoreClient window = new PizzaStoreClient();
					window.frmPizzaOrderingSystem.setVisible(true);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	private void initialize() throws RemoteException
	{
		//Window
		frmPizzaOrderingSystem = new JFrame();
		frmPizzaOrderingSystem.setTitle("Pizza Ordering System");
		frmPizzaOrderingSystem.setBounds(100, 100, 854, 469);
		frmPizzaOrderingSystem.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPizzaOrderingSystem.getContentPane().setLayout(null);
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 846, 431);
		frmPizzaOrderingSystem.getContentPane().add(tabbedPane);

		//Login Tab
		JPanel loginPanel = new JPanel();
		tabbedPane.addTab("Employee", null, loginPanel, null);
		loginPanel.setLayout(null);
		//User Drop Down List
		loginBox = new JComboBox();
		loginBox.setBounds(80, 112, 231, 20);
		loginPanel.add(loginBox);
		//Login Button
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(80, 143, 89, 23);
		loginPanel.add(btnLogin);
		//Label
		JLabel lblLoginAsAn = new JLabel("Select User Mode");
		lblLoginAsAn.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLoginAsAn.setBounds(80, 65, 164, 20);
		loginPanel.add(lblLoginAsAn);
		//Add User Types
		ArrayList<String> employees = new ArrayList<String>();
		employees = model.getEmployees();
		for(int i = 0; i < employees.size(); i++)
		{
			loginBox.addItem(employees.get(i));
		}

		//Order Tab
		final JPanel makePanel = new JPanel();
		tabbedPane.addTab("Order", null, makePanel, null);
		makePanel.setLayout(null);
		pizzaPane = new JScrollPane();
		pizzaPane.setBounds(36, 48, 173, 123);
		makePanel.add(pizzaPane);
		//Add Pizza
		final JLabel lblPizzaMenu = new JLabel("Pizza Menu");
		lblPizzaMenu.setLabelFor(pizzaPane);
		pizzaPane.setColumnHeaderView(lblPizzaMenu);
		pizzaList = new JList(model.getPizzaList());
		pizzaList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pizzaPane.setViewportView(pizzaList);
		JButton btnAddPizza = new JButton("Add Pizza");
		btnAddPizza.setBounds(120, 182, 89, 23);
		makePanel.add(btnAddPizza);
		//Add Drink
		drinkPane = new JScrollPane();
		drinkPane.setBounds(251, 48, 173, 123);
		makePanel.add(drinkPane);
		JLabel lblDrinkMenu = new JLabel("Drink Menu");
		drinkPane.setColumnHeaderView(lblDrinkMenu);
		drinkList = new JList(model.getSodaList());
		drinkList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		drinkPane.setViewportView(drinkList);
		JButton btnAddSoda = new JButton("Add Soda");
		btnAddSoda.setBounds(335, 182, 89, 23);
		makePanel.add(btnAddSoda);
		//Add Other
		otherPane = new JScrollPane();
		otherPane.setBounds(464, 48, 173, 123);
		makePanel.add(otherPane);
		JLabel lblOtherItems = new JLabel("Other Items");
		otherPane.setColumnHeaderView(lblOtherItems);
		otherList = new JList(model.getOtherList());
		otherList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		otherPane.setViewportView(otherList);
		JButton btnAddItem = new JButton("Add Item");
		btnAddItem.setBounds(548, 182, 89, 23);
		makePanel.add(btnAddItem);
		//Order Contents
		orderContents = new JTextArea();
		orderContents.setEditable(false);
		orderContents.setBounds(36, 244, 496, 115);
		makePanel.add(orderContents);
		JLabel lblOrderContents = new JLabel("Order Contents");
		lblOrderContents.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblOrderContents.setBounds(36, 216, 97, 17);
		makePanel.add(lblOrderContents);
		//Submit Order
		JButton btnOrderSubmit = new JButton("Submit");
		btnOrderSubmit.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnOrderSubmit.setBounds(742, 357, 89, 35);
		makePanel.add(btnOrderSubmit);
		totalField = new JTextField();
		totalField.setBounds(742, 326, 86, 20);
		makePanel.add(totalField);
		totalField.setColumns(10);
		JLabel lblTotal = new JLabel("Total -");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(686, 329, 46, 14);
		makePanel.add(lblTotal);
		//Payment
		payField = new JTextField();
		payField.setBounds(745, 295, 86, 20);
		makePanel.add(payField);
		payField.setColumns(10);
		JLabel lblPaymentAmount = new JLabel("Payment Amount (0 for Credit) - ");
		lblPaymentAmount.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPaymentAmount.setBounds(542, 298, 190, 14);
		makePanel.add(lblPaymentAmount);
		//Phone
		phoneField = new JTextField();
		phoneField.setBounds(720, 264, 111, 20);
		makePanel.add(phoneField);
		phoneField.setColumns(10);
		JLabel lblPhoneNumber = new JLabel("Phone Number -");
		lblPhoneNumber.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPhoneNumber.setBounds(621, 267, 97, 14);
		makePanel.add(lblPhoneNumber);
		//Address
		addressField = new JTextField();
		addressField.setBounds(704, 164, 127, 20);
		makePanel.add(addressField);
		addressField.setColumns(10);
		JLabel lblAddress = new JLabel("Address -");
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAddress.setBounds(647, 167, 63, 14);
		makePanel.add(lblAddress);
		//Reset
		JButton btnReset = new JButton("Reset");
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnReset.setBounds(548, 357, 89, 35);
		makePanel.add(btnReset);
		//Quantity
		quantityField = new JTextField();
		quantityField.setBounds(745, 62, 86, 20);
		makePanel.add(quantityField);
		quantityField.setColumns(10);
		JLabel lblQuantity = new JLabel("Quantity :");
		lblQuantity.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblQuantity.setBounds(669, 65, 63, 14);
		makePanel.add(lblQuantity);
		//Name
		custName = new JTextField();
		custName.setBounds(745, 133, 86, 20);
		makePanel.add(custName);
		custName.setColumns(10);
		JLabel lblcustName = new JLabel("Name :");
		lblcustName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblcustName.setBounds(686, 136, 46, 14);
		makePanel.add(lblcustName);

		//View Order Tab
		final JPanel orderPanel = new JPanel();
		tabbedPane.addTab("View Order", null, orderPanel, null);
		orderPanel.setLayout(null);
		currentOrderPane = new JScrollPane();
		currentOrderPane.setBounds(47, 45, 309, 331);
		orderPanel.add(currentOrderPane);
		//Order List
		JLabel lblOrderList = new JLabel("Order List");
		lblOrderList.setFont(new Font("Tahoma", Font.BOLD, 12));
		currentOrderPane.setColumnHeaderView(lblOrderList);
		orderList = new JList();
		orderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		currentOrderPane.setViewportView(orderList);
		//Finished Button
		final JButton btnMarkFinished = new JButton("Mark Finished");
		btnMarkFinished.setBounds(366, 316, 117, 23);
		orderPanel.add(btnMarkFinished);
		//Delete Button
		final JButton btnDeleteOrder = new JButton("Detele Order");
		btnDeleteOrder.setBounds(366, 350, 117, 23);
		orderPanel.add(btnDeleteOrder);

		//Edit Menu Tab
		final JPanel menuPanel = new JPanel();
		tabbedPane.addTab("Edit Menu", null, menuPanel, null);
		menuPanel.setLayout(null);
		//Clear Special
		JButton btnClearMenuSpecial = new JButton("Clear Specials");
		btnClearMenuSpecial.setBounds(168, 356, 133, 23);
		menuPanel.add(btnClearMenuSpecial);
		//Add Item
		nameField = new JTextField();
		nameField.setBounds(648, 40, 133, 20);
		menuPanel.add(nameField);
		nameField.setColumns(10);
		JLabel lblItemName = new JLabel("Item Name:");
		lblItemName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblItemName.setBounds(563, 43, 75, 14);
		menuPanel.add(lblItemName);
		descField = new JTextField();
		descField.setBounds(648, 71, 133, 62);
		menuPanel.add(descField);
		descField.setColumns(10);
		priceField = new JTextField();
		priceField.setBounds(648, 144, 133, 20);
		menuPanel.add(priceField);
		priceField.setColumns(10);
		JLabel lblItemDescription = new JLabel("Item Description:");
		lblItemDescription.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblItemDescription.setBounds(534, 76, 104, 14);
		menuPanel.add(lblItemDescription);
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(601, 147, 37, 14);
		menuPanel.add(lblPrice);
		JButton btnAddMenu = new JButton("Add Item");
		btnAddMenu.setBounds(692, 356, 89, 23);
		menuPanel.add(btnAddMenu);
		typeBox = new JComboBox();
		typeBox.setBounds(600, 170, 133, 20);
		menuPanel.add(typeBox);
		typeBox.addItem("Pizza");
		typeBox.addItem("Drink");
		typeBox.addItem("Other");

		//Pizza List
		editPizzaPane = new JScrollPane();
		editPizzaPane.setBounds(10, 43, 133, 127);
		menuPanel.add(editPizzaPane);
		JLabel lblPizzaMenu_1 = new JLabel("Pizza menu");
		editPizzaPane.setColumnHeaderView(lblPizzaMenu_1);
		editPizzaList = new JList();
		editPizzaList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		editPizzaPane.setViewportView(editPizzaList);
		//Drinks List
		editDrinkPane = new JScrollPane();
		editDrinkPane.setBounds(168, 43, 144, 127);
		menuPanel.add(editDrinkPane);
		JLabel lblDrinkMenu_1 = new JLabel("Soda menu");
		editDrinkPane.setColumnHeaderView(lblDrinkMenu_1);
		editDrinkList = new JList();
		editDrinkList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		editDrinkPane.setViewportView(editDrinkList);
		//Other List
		editOtherPane = new JScrollPane();
		editOtherPane.setBounds(338, 43, 144, 127);
		menuPanel.add(editOtherPane);
		JLabel lblOtherMenu = new JLabel("Other menu");
		editOtherPane.setColumnHeaderView(lblOtherMenu);
		editOtherList = new JList();
		editOtherList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		editOtherPane.setViewportView(editOtherList);
		//Remove Pizza
		JButton removePizzaBtn = new JButton("Remove Pizza");
		removePizzaBtn.setBounds(10, 205, 133, 23);
		menuPanel.add(removePizzaBtn);
		//Set Special Pizza
		JButton pizzaSpecialBtn = new JButton("Make Menu Special");
		pizzaSpecialBtn.setBounds(10, 239, 133, 23);
		menuPanel.add(pizzaSpecialBtn);
		//Remove Drink
		JButton removeDrinkBtn = new JButton("Remove Soda");
		removeDrinkBtn.setBounds(168, 205, 144, 23);
		menuPanel.add(removeDrinkBtn);
		//Set Special Drink
		JButton sodaSpecialBtn = new JButton("Make Menu Special");
		sodaSpecialBtn.setBounds(168, 239, 144, 23);
		menuPanel.add(sodaSpecialBtn);
		//Remove Other
		JButton removeOtherBtn = new JButton("Remove Item");
		removeOtherBtn.setBounds(338, 205, 144, 23);
		menuPanel.add(removeOtherBtn);
		JButton otherSpecialBtn = new JButton("Make Menu Special");
		otherSpecialBtn.setBounds(338, 239, 144, 23);
		menuPanel.add(otherSpecialBtn);
		//Start in Kiosk Mode
		tabbedPane.remove(makePanel);
		tabbedPane.remove(orderPanel);
		tabbedPane.remove(menuPanel);
		tabbedPane.addTab("Order", makePanel);

		//Tab Listener
		tabbedPane.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{   //Detect if tab has changed
				int tab;
				tab = tabbedPane.getSelectedIndex();
				switch(tab)
				{
					case 0:
						//Employee tab
						break;
					case 1:
						//Order page
						try
						{
							model.ClearOrder();
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						try
						{
							pizzaList = new JList(model.getPizzaList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						try
						{
							drinkList = new JList(model.getSodaList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						try
						{
							otherList = new JList(model.getOtherList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}

						pizzaPane.setViewportView(pizzaList);   //Refresh pane
						pizzaPane.revalidate();
						pizzaPane.repaint();

						drinkPane.setViewportView(drinkList);      //Refresh pane
						drinkPane.revalidate();
						drinkPane.repaint();

						otherPane.setViewportView(otherList);   //Refresh pane
						otherPane.revalidate();
						otherPane.repaint();

						//Reset fields
						orderContents.setText("");
						totalField.setText("");
						payField.setText("");

						break;
					case 2:
						//View Order
						try
						{
							orderList = new JList(model.getOrdersList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						currentOrderPane.setViewportView(orderList);
						currentOrderPane.revalidate();
						currentOrderPane.repaint();

						break;
					case 3:
						//Edit menu
						try
						{
							editPizzaList = new JList(model.getPizzaList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						try
						{
							editDrinkList = new JList(model.getSodaList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						try
						{
							editOtherList = new JList(model.getOtherList());
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						editPizzaPane.setViewportView(editPizzaList);
						editPizzaPane.revalidate();
						editPizzaPane.repaint();

						editDrinkPane.setViewportView(editDrinkList);
						editDrinkPane.revalidate();
						editDrinkPane.repaint();

						editOtherPane.setViewportView(editOtherList);
						editOtherPane.revalidate();
						editOtherPane.repaint();

						//Reset fields
						nameField.setText("");
						descField.setText("");
						priceField.setText("");
						break;
				}
			}
		});

		//Login Tab
		btnLogin.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				int type = 0;
				type = loginBox.getSelectedIndex();
				if(type == 0)
				{
					//Kiosk
					tabbedPane.remove(makePanel);
					tabbedPane.remove(orderPanel);
					tabbedPane.remove(menuPanel);
					tabbedPane.addTab("Order", makePanel);
				}
				else
				{
					switch(type)
					{
						case 0: //Employee
							tabbedPane.remove(makePanel);
							tabbedPane.remove(orderPanel);
							tabbedPane.remove(menuPanel);
							tabbedPane.addTab("Order", makePanel);
							tabbedPane.addTab("View Orders", orderPanel);
							btnMarkFinished.setVisible(false);
							btnDeleteOrder.setVisible(false);
							break;
						case 1: //Chef
							tabbedPane.remove(makePanel);
							tabbedPane.remove(orderPanel);
							tabbedPane.remove(menuPanel);
							tabbedPane.addTab("Order", makePanel);
							tabbedPane.addTab("View Orders", orderPanel);
							btnMarkFinished.setVisible(true);
							btnDeleteOrder.setVisible(false);
							break;
						case 2: //Manager
							tabbedPane.remove(makePanel);
							tabbedPane.remove(orderPanel);
							tabbedPane.remove(menuPanel);
							tabbedPane.addTab("Order", makePanel);
							tabbedPane.addTab("View Orders", orderPanel);
							btnMarkFinished.setVisible(true);
							btnDeleteOrder.setVisible(true);
							tabbedPane.addTab("Edit Menu", menuPanel);
							break;
					}
				}

			}
		});
		//Order Tab
		btnAddPizza.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(!pizzaList.isSelectionEmpty())
				{
					String holder = quantityField.getText();
					int parsed = 0;
					if(holder.compareTo("") == 0)
					{
						parsed = 1;
					}
					else
					{
						parsed = Integer.parseInt(quantityField.getText());
					}
					try
					{
						model.addPizzaItem(pizzaList.getSelectedIndex(), parsed);
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						int total;
						total = model.GetTotal();
						String totalString = "$" + (float) total / 100.0;
						totalField.setText(totalString);
						quantityField.setText("");
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						orderContents.setText(model.SelectedItems());
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
				}
			}
		});
		btnAddSoda.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(!drinkList.isSelectionEmpty())
				{
					String holder = quantityField.getText();
					int parsed = 0;
					if(holder.compareTo("") == 0)
					{
						parsed = 1;
					}
					else
					{
						parsed = Integer.parseInt(quantityField.getText());
					}
					try
					{
						model.addSodaItem(drinkList.getSelectedIndex(), parsed);
					}
					catch(RemoteException e2)
					{
						e2.printStackTrace();
					}
					try
					{
						int total;
						total = model.GetTotal();
						String totalString = "$" + (float) total / 100.0;
						totalField.setText(totalString);
						quantityField.setText("");
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}

					try
					{
						orderContents.setText(model.SelectedItems());
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
				}
			}
		});
		btnAddItem.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{

				if(!otherList.isSelectionEmpty())
				{
					String holder = quantityField.getText();
					int parsed = 0;
					if(holder.compareTo("") == 0)
					{
						parsed = 1;
					}
					else
					{
						parsed = Integer.parseInt(quantityField.getText());
					}
					try
					{
						model.addOtherItem(otherList.getSelectedIndex(), parsed);
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						int total;
						total = model.GetTotal();
						String totalString = "$" + (float) total / 100.0;
						totalField.setText(totalString);
						quantityField.setText("");
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						orderContents.setText(model.SelectedItems());
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
				}
			}
		});
		btnOrderSubmit.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(orderContents.getText() != "")
				{
					try
					{
						model.CompleteOrder(custName.getText(), "", "", "", "", "", (int) Double.parseDouble(payField.getText()) * 100);
						custName.setText("");
						addressField.setText("");
						phoneField.setText("");
						payField.setText("");
						totalField.setText("");
						orderContents.setText("");
						pizzaList.setSelectedIndex(0);
						drinkList.setSelectedIndex(0);
						otherList.setSelectedIndex(0);
						model.ClearOrder();
					}
					catch(Exception ex)
					{
						DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, new Locale("en", "EN"));
						String formattedDate = df.format(new Date());
						try
						{
							model.CompleteOrder("Kiosk Order " + formattedDate, addressField.getText(), "Fort Collins", "Colorado", "80525", phoneField.getText(), 0);
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
						custName.setText("");
						addressField.setText("");
						phoneField.setText("");
						payField.setText("");
						orderContents.setText("");
						totalField.setText("");
						pizzaList.setSelectedIndex(0);
						drinkList.setSelectedIndex(0);
						otherList.setSelectedIndex(0);
						try
						{
							model.ClearOrder();
						}
						catch(RemoteException e1)
						{
							e1.printStackTrace();
						}
					}
				}
			}
		});
		btnReset.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				//Reset fields
				custName.setText("");
				addressField.setText("");
				phoneField.setText("");
				payField.setText("");
				orderContents.setText("");
				totalField.setText("");
				pizzaList.setSelectedIndex(0);
				drinkList.setSelectedIndex(0);
				otherList.setSelectedIndex(0);
				try
				{
					model.ClearOrder();
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
			}
		});

		//View Orders Tab
		btnMarkFinished.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.markOrderFinished(orderList.getSelectedIndex());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				try
				{
					orderList = new JList(model.getOrdersList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				currentOrderPane.setViewportView(orderList);
				currentOrderPane.revalidate();
				currentOrderPane.repaint();
			}
		});
		btnDeleteOrder.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.removeOrder(orderList.getSelectedIndex());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				try
				{
					orderList = new JList(model.getOrdersList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				currentOrderPane.setViewportView(orderList);
				currentOrderPane.revalidate();
				currentOrderPane.repaint();
			}
		});

		//Edit Menu Tab
		removePizzaBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				if(!editPizzaList.isSelectionEmpty())
				{
					try
					{
						model.RemovePizza(editPizzaList.getSelectedIndex());
					}
					catch(RemoteException e)
					{
						e.printStackTrace();
					}
					try
					{
						editPizzaList = new JList(model.getPizzaList());
					}
					catch(RemoteException e)
					{
						e.printStackTrace();
					}    //return string[] in constructor
					editPizzaPane.setViewportView(editPizzaList);
					editPizzaPane.revalidate();
					editPizzaPane.repaint();
				}
			}
		});
		//Remove Soda Button
		removeDrinkBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				if(!editDrinkList.isSelectionEmpty())
				{
					try
					{
						model.RemoveSoda(editDrinkList.getSelectedIndex());
					}
					catch(RemoteException e)
					{
						e.printStackTrace();
					}
					try
					{
						editDrinkList = new JList(model.getSodaList());
					}
					catch(RemoteException e)
					{
						e.printStackTrace();
					}      //return string[] in constructor
					editDrinkPane.setViewportView(editDrinkList);
					editDrinkPane.revalidate();
					editDrinkPane.repaint();
				}
			}
		});
		//Remove Other Button
		removeOtherBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if(!editOtherList.isSelectionEmpty())
				{
					try
					{
						model.RemoveOther(editOtherList.getSelectedIndex());
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}
					try
					{
						editOtherList = new JList(model.getOtherList());
					}
					catch(RemoteException e1)
					{
						e1.printStackTrace();
					}      //return string[] in constructor
					editOtherPane.setViewportView(editOtherList);
					editOtherPane.revalidate();
					editOtherPane.repaint();
				}
			}
		});
		//Pizza Special
		pizzaSpecialBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.setPizzaSpecial(editPizzaList.getSelectedIndex());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				//Edit menu
				try
				{
					editPizzaList = new JList(model.getPizzaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}    //return string[] in constructor
				try
				{
					editDrinkList = new JList(model.getSodaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				try
				{
					editOtherList = new JList(model.getOtherList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor

				editPizzaPane.setViewportView(editPizzaList);
				editPizzaPane.revalidate();
				editPizzaPane.repaint();

				editDrinkPane.setViewportView(editDrinkList);
				editDrinkPane.revalidate();
				editDrinkPane.repaint();

				editOtherPane.setViewportView(editOtherList);
				editOtherPane.revalidate();
				editOtherPane.repaint();

				//Reset fields
				nameField.setText("");
				descField.setText("");
				priceField.setText("");
			}
		});
		//Drink Special
		sodaSpecialBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.setDrinkSpecial(editDrinkList.getSelectedIndex());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				//Edit menu
				try
				{
					editPizzaList = new JList(model.getPizzaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}    //return string[] in constructor
				try
				{
					editDrinkList = new JList(model.getSodaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				try
				{
					editOtherList = new JList(model.getOtherList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor

				editPizzaPane.setViewportView(editPizzaList);
				editPizzaPane.revalidate();
				editPizzaPane.repaint();

				editDrinkPane.setViewportView(editDrinkList);
				editDrinkPane.revalidate();
				editDrinkPane.repaint();

				editOtherPane.setViewportView(editOtherList);
				editOtherPane.revalidate();
				editOtherPane.repaint();

				//Reset fields
				nameField.setText("");
				descField.setText("");
				priceField.setText("");
			}
		});
		otherSpecialBtn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.setOtherSpecial(editOtherList.getSelectedIndex());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				//Edit menu
				try
				{
					editPizzaList = new JList(model.getPizzaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}    //return string[] in constructor
				try
				{
					editDrinkList = new JList(model.getSodaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				try
				{
					editOtherList = new JList(model.getOtherList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor

				editPizzaPane.setViewportView(editPizzaList);
				editPizzaPane.revalidate();
				editPizzaPane.repaint();

				editDrinkPane.setViewportView(editDrinkList);
				editDrinkPane.revalidate();
				editDrinkPane.repaint();

				editOtherPane.setViewportView(editOtherList);
				editOtherPane.revalidate();
				editOtherPane.repaint();

				//Reset fields
				nameField.setText("");
				descField.setText("");
				priceField.setText("");
			}
		});
		btnClearMenuSpecial.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.clearSpecials();
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				//Edit menu
				try
				{
					editPizzaList = new JList(model.getPizzaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}    //return string[] in constructor
				try
				{
					editDrinkList = new JList(model.getSodaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				try
				{
					editOtherList = new JList(model.getOtherList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor

				editPizzaPane.setViewportView(editPizzaList);
				editPizzaPane.revalidate();
				editPizzaPane.repaint();

				editDrinkPane.setViewportView(editDrinkList);
				editDrinkPane.revalidate();
				editDrinkPane.repaint();

				editOtherPane.setViewportView(editOtherList);
				editOtherPane.revalidate();
				editOtherPane.repaint();

				//Reset fields
				nameField.setText("");
				descField.setText("");
				priceField.setText("");
			}
		});
		btnAddMenu.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					model.addItem(nameField.getText(), descField.getText(), (int) Double.parseDouble(priceField.getText()) * 100, typeBox.getSelectedIndex());
				}
				catch(NumberFormatException e1)
				{
					e1.printStackTrace();
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}
				nameField.setText("");
				descField.setText("");
				priceField.setText("");
				typeBox.setSelectedIndex(0);
				//Refresh
				try
				{
					editPizzaList = new JList(model.getPizzaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}    //return string[] in constructor
				editPizzaPane.setViewportView(editPizzaList);
				editPizzaPane.revalidate();
				editPizzaPane.repaint();
				try
				{
					editDrinkList = new JList(model.getSodaList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				editDrinkPane.setViewportView(editDrinkList);
				editDrinkPane.revalidate();
				editDrinkPane.repaint();
				try
				{
					editOtherList = new JList(model.getOtherList());
				}
				catch(RemoteException e1)
				{
					e1.printStackTrace();
				}      //return string[] in constructor
				editOtherPane.setViewportView(editOtherList);
				editOtherPane.revalidate();
				editOtherPane.repaint();
			}
		});

	}
}
