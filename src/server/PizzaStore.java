package server;

import java.util.ArrayList;

public interface PizzaStore extends java.rmi.Remote
{
	public String[] getOrdersList()
			throws java.rmi.RemoteException;

	public String[] getPizzaList()
			throws java.rmi.RemoteException;

	public String[] getSodaList()
			throws java.rmi.RemoteException;

	public String[] getOtherList()
			throws java.rmi.RemoteException;

	public void RemovePizza(int index)
			throws java.rmi.RemoteException;

	public void RemoveSoda(int index)
			throws java.rmi.RemoteException;

	public void RemoveOther(int index)
			throws java.rmi.RemoteException;

	public String CompleteOrder(String name, String address, String city, String state, String zipCode, String phoneNumber, int paidAmount)
			throws java.rmi.RemoteException;

	public void ClearOrder()
			throws java.rmi.RemoteException;

	public int GetTotal()
			throws java.rmi.RemoteException;

	public void addPizzaItem(int arrayLocation, int quantity)
			throws java.rmi.RemoteException;

	public void addSodaItem(int arrayLocation, int quantity)
			throws java.rmi.RemoteException;

	public void addOtherItem(int arrayLocation, int quantity)
			throws java.rmi.RemoteException;

	public String SelectedItems()
			throws java.rmi.RemoteException;

	public void setPizzaSpecial(int arrayLocation)
			throws java.rmi.RemoteException;

	public void setDrinkSpecial(int arrayLocation)
			throws java.rmi.RemoteException;

	public void setOtherSpecial(int arrayLocation)
			throws java.rmi.RemoteException;

	public void clearSpecials()
			throws java.rmi.RemoteException;

	public void addItem(String name, String desc, int price, int type)
			throws java.rmi.RemoteException;

	public int employeeType(int arrayLocation)
			throws java.rmi.RemoteException;

	public void addEmployee(String name, String address, String city, String state, String zipcode, String phone, String login, String password, int type)
			throws java.rmi.RemoteException;

	public ArrayList<String> getEmployees()
			throws java.rmi.RemoteException;

	public void markOrderFinished(int arrayLocation)
			throws java.rmi.RemoteException;

	public void removeOrder(int arrayLocation)
			throws java.rmi.RemoteException;
}
